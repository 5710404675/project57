import pygame
import random
import time

class Image :
    def __init__(self,file) :
        self.surface = pygame.image.load(file).convert_alpha()
        self.rect = self.surface.get_rect()

    def changeFile(file) :
        self.surface = pygame.image.load(file).convert_alpha()

    def render(self,position) :
        position_image = self.rect.copy()
        position_image.topleft = position
        Game.screen.blit(self.surface, position_image)

class Text :
    def __init__(self,msg,color,size = 25) :
        self.msg = msg
        self.color = color
        self.font = pygame.font.SysFont("Alice and the Wicked Monster", size)
        self.surface = self.font.render(self.msg,True,self.color)
        self.rect = self.surface.get_rect()

    def change_color(self,color,area,position):
        self.color = color
        if area == "center" :
            return self.to_screen_center(position)
        else :
            return self.to_screen_normal(position)
        
    def to_screen_center(self, y_screen = 0) :
        text_surface, text_rect = self.surface , self.rect
        text_rect.center = (Game.screen_width//2), (Game.screen_height//2)+ y_screen
        Game.screen.blit(text_surface, text_rect)

    def to_screen_normal(self, x_screen, y_screen) :
        text_surface, text_rect = self.surface , self.rect
        text_rect.topleft = x_screen, y_screen
        Game.screen.blit(text_surface, text_rect)

class Curser : #------------------------#
    def __init__(self) :
        self.cur = pygame.mouse.get_pos()
        self.click = pygame.mouse.get_pressed()
        
class Song :
    def load(self,file) :
        return pygame.mixer.music.load(file)
    def play(self,time = -1) :
        return pygame.mixer.music.play(time)

    def pause(self) :
        return pygame.mixer.music.pause()

    def unpause(self) :
        return pygame.mixer.music.unpause()

class SoundEffect :
    def __init__(self,file) :
        self.file = pygame.mixer.Sound(file)

    def play(self) :
        return self.file.play()

class Life :
    def __init__(self) :
        self.one = Image("Life_1.png")
        self.two = Image("Life_2.png")
        self.three = Image("Life_3.png")

        self.colors = []
        self.positios = []

    def shorter(self) :
        self.colors.pop(0)
        self.positions.pop(0)

    def render(self) :
        for position in range(0,len(self.positions)) :
            Game.screen.blit(self.colors[position], self.positions[position])
    
class Snake :
    def __init__(self) :
        self.top = Image("Head_Snake.png")
        self.mid = Image("Square_Green_Snake.png")
        self.bot = Image("Square_Blue_Snake.png")
        self.tail = Image("Circle_Snake.png")

        self.colors = []
        self.positions = []

        self.directions = [1,0]

    def longer(self) :
        width = self.tail.rect.width
        height = self.tail.rect.height
        x = self.positions[-1].x
        y = self.positions[-1].y
        if self.positions[-2].x == x :
            if self.positions[-2].y > y :
                y = y - height
            else :
                y = y + height
        elif self.positions[-2].y == y :
            if self.positions[-2].x > x :
                x = x - width
            else :
                x = x + width
        tail = self.tail.rect.move(x, y)
        self.positions.append(tail)
        self.colors.append(self.tail.surface)

    def shorter(self) :
        self.colors.pop(-1)
        self.positions.pop(-1)

    def rotate(self) :
        if self.directions[1] == -1 :
            top_rotate = self.top.surface
        elif self.directions[1] == 1 :
            top_rotate = pygame.transform.rotate(self.top.surface, 180)
        elif self.directions[0] == -1 :
            top_rotate = pygame.transform.rotate(self.top.surface, 90)
        elif self.directions[0] == 1 :
            top_rotate = pygame.transform.rotate(self.top.surface, 270)
        self.colors[0] = top_rotate

    def move(self) :
        tail = self.positions.pop(-1)
        width = tail.width
        x = self.positions[0].x + (width * self.directions[0])
        if x >= Game.screen_width :
            x = x - Game.screen_width
        elif x < 0 :
            x = Game.screen_width + x
        y = self.positions[0].y + (width * self.directions[1])
        if y >= Game.screen_height - 50 :
            y = y - (Game.screen_height - 50)
        elif y < 0 :
            y = (Game.screen_height - 50) + y
        tail.topleft = (x, y)
        self.positions.insert(0, tail)

    def event_handler(self,event) :
        if event.type == pygame.KEYDOWN :
            if event.key == pygame.K_UP and self.directions[1] == 0 :
                self.directions[0] = 0
                self.directions[1] = -1 
            elif event.key == pygame.K_DOWN and self.directions[1] == 0 :
                self.directions[0] = 0
                self.directions[1] = 1
            elif event.key == pygame.K_LEFT and self.directions[0] == 0 :
                self.directions[0] = -1
                self.directions[1] = 0
            elif event.key == pygame.K_RIGHT and self.directions[0] == 0 :
                self.directions[0] = 1
                self.directions[1] = 0 

    def render(self) :
        for position in range(0,len(self.positions)) :
            Game.screen.blit(self.colors[position], self.positions[position])

class Item :
    def random(self) :
        randX = random.randrange(0, Game.screen_width - 50)
        randY = random.randrange(0, Game.screen_height - 50)
        new_rect = self.item.rect
        new_rect.topleft = (randX,randY)
        return new_rect, randX, randY

    def render(self) :
        for position in self.positions :
            Game.screen.blit(self.item.surface, position)

class ClassicItem(Item) :
    def __init__(self) :
        self.item = Image("Classic_Item.png")
        self.positions = []

    def setup(self,classic_wall,rotate_wall,snake) :
        if len(self.positions) == 0 :
            new_rect, randX, randY = self.random()
            while new_rect.collidelist(classic_wall) >= 0 or new_rect.collidelist(rotate_wall) >= 0 or new_rect.collidelist(snake) >= 0 or randX%50 != 0 or randY%50 != 0 :
                new_rect, randX, randY = self.random()
            self.positions.append(new_rect)

class SuperItem(Item) :
    def __init__(self) :
        self.item = Image("Super_Item.png")
        self.positions = []

    def setup(self,classic_item,classic_wall,rotate_wall,snake) :
        if len(self.positions) != 1 :
            new_rect, randX, randY = self.random()
            while new_rect.collidelist(classic_item) >= 0 or new_rect.collidelist(classic_wall) >= 0 or new_rect.collidelist(rotate_wall) >= 0 or new_rect.collidelist(snake) >= 0 or randX%50 != 0 or randY%50 != 0 :
                new_rect, randX, randY = self.random()
            self.positions.append(new_rect)

class Wall :
    def setup(self,num_range,topleft) :
        for num in num_range :
            new_rect = self.item.rect.copy()
            new_rect.topleft = topleft
            self.positions.append(new_rect)

    def setnum(self,lst_range) :
        for num in lst_range :
            return 

    def render(self) :
        for position in self.positions :
            Game.screen.blit(self.item.surface, position)
        
class ClassicWall(Wall) :
    def __init__(self) :
        self.item = Image("Wall.png")
        self.positions = []

class RotateWall(Wall) :
    def __init__(self) :
        self.item = Image("Wall_Rotate.png")
        self.positions = []

class Map :
    def __init__(self) :
        self.classic_wall = ClassicWall()
        self.rotate_wall = RotateWall()
        
    def mode1(self) :
        for num in range(0, (Game.screen_width//100)+1):
            self.classic_wall.setup(range(0, (Game.screen_width//100)+1), (num*100,550))
            self.classic_wall.setup(range(0, (Game.screen_width//100)+1), (num*100,0))
        
    def mode2(self) :
        for num in range(2, 6):
            self.classic_wall.setup(range(2, 6), (num*100,150))
            self.classic_wall.setup(range(2, 6), (num*100,400))
        for num in [-50,50,450,550]:
            self.rotate_wall.setup([-50,50,450,550], (750,num))
            self.rotate_wall.setup([-50,50,450,550], (0,num))
        for num in [0,550] :
            self.classic_wall.setup([0,550], (50,num))
            self.classic_wall.setup([0,550], (650,num))

    def mode3(self) :
        for num in [1] :
            self.rotate_wall.setup([1], (250,-50))
            self.classic_wall.setup([1], (-50,200))
        for num in range(0, 2) :
            self.rotate_wall.setup(range(0, 2), (250,50+num*100))
        for num in range(1, 6) :
            self.classic_wall.setup(range(1, 6), (200+num*100,0))
        for num in range(4, 6) :
            self.rotate_wall.setup(range(4, 6), (400,num*100))
        for num in range(1, 5) :
            self.classic_wall.setup(range(1, 5), (300+num*100,200))
        for num in range(0, 2) :
            self.classic_wall.setup(range(0, 2), (50+num*100,200))
        for num in range(0, 3) :
            self.classic_wall.setup(range(0, 3), (num*100,400))
        for num in range(5, 9) :
            self.classic_wall.setup(range(5, 9), (num*100-50,400))

    def render(self) :
        self.classic_wall.render()
        self.rotate_wall.render()

class Game :
    screen_width = 800
    screen_height = 650
    pygame.init()
    screen = pygame.display.set_mode((screen_width, screen_height))
    pygame.display.set_caption("Feed the snake")

    def __init__(self) :
        self.clock = pygame.time.Clock()

        self.game_exit = False
        self.game_main = True
        self.game_mode = False
        self.game_how = False
        self.game_start = False
        self.game_pause = False
        self.game_over = False
        
        self.set_mode = 0 
        self.score = 0
        self.eat_item = 0

        self.background = Image("Background_Classic.png") #background
        
        self.score_image = Image("Score_game.png") #score

        self.life = Life() #life
        self.text_life = Text("LIFE",(0,0,0),35)

        self.song = Song()

        self.main_image = Image("pic_menu.png") #main_menu_object
        self.text_main_1 = Text("FEED THE SNAKE",(255,0,0),100)
        self.text_main_2 = Text("Play game",(120,120,120),50)
        self.text_main_3 = Text("Play game",(0,0,0),50)
        self.text_main_4 = Text("How to play",(120,120,120),50)
        self.text_main_5 = Text("How to play",(0,0,0),50)
        self.text_main_6 = Text("Quit",(120,120,120),50)
        self.text_main_7 = Text("Quit",(0,0,0),50)

        self.map_1_image = Image("Map_1.png") #mode_object
        self.map_1_c_image = Image("Map_1_click.png")
        self.map_2_image = Image("Map_2.png")
        self.map_2_c_image = Image("Map_2_click.png")
        self.map_3_image = Image("Map_3.png")
        self.map_3_c_image = Image("Map_3_click.png")
        self.text_mode_1 = Text("Mode Game",(255,0,0),100)
        self.text_mode_2 = Text("Normal",(120,120,120),50)
        self.text_mode_3 = Text("Normal",(0,0,0),50)
        self.text_mode_4 = Text("Hard",(120,120,120),50)
        self.text_mode_5 = Text("Hard",(0,0,0),50)
        self.text_mode_6 = Text("Insane",(120,120,120),50)
        self.text_mode_7 = Text("Insane",(0,0,0),50)
        self.text_mode_8 = Text("Back to menu",(125,125,125),50)
        self.text_mode_9 = Text("Back to menu",(255,0,0))
        self.mode_sound_effect = SoundEffect("mode_select_sound.wav")

        self.text_how_1 = Text("How to play",(255,0,0),100) #how_object
        self.text_how_2 = Text("You can control the snake using the arrows.",(0,0,0),35)
        self.text_how_3 = Text("Pressing P will pause the game.",(0,0,0),35)
        self.text_how_4 = Text("There’s two kinds of item in the game : normal apple and special apple,",(0,0,0),35)
        self.text_how_5 = Text("the first will make your snake grow",(0,0,0),35)
        self.text_how_6 = Text("while the latter will give you bonus score and a good or bad effect.",(0,0,0),35)
        self.text_how_7 = Text("You’ll start with three lives.",(0,0,0),35)
        self.text_how_8 = Text("You can crash into yourself three times but if you hit the wall",(0,0,0),35)
        self.text_how_9 = Text("you’ll be dead instantly !!!",(0,0,0),35)
        self.text_how_10 = Text("Back to menu",(120,120,120),50)
        self.text_how_11 = Text("Back to menu",(255,0,0),35)

        self.pause_image = Image("pic_pause.png") #pause_object
        self.text_pause_1 = Text("Pause",(255,0,0),100)
        self.text_pause_2 = Text("Resume",(120,120,120),50)
        self.text_pause_3 = Text("Resume",(0,0,0),50)
        self.text_pause_4 = Text("Back to menu",(120,120,120),50)
        self.text_pause_5 = Text("Back to menu",(0,0,0),50)

        self.over_image = Image("pic_gameOver.png") #over_oject
        self.text_over_1 = Text("Game Over",(255,0,0),100)
        self.text_over_3 = Text("Play again",(120,120,120),50)
        self.text_over_4 = Text("Play again",(0,0,0),50)
        self.text_over_5 = Text("Back to menu",(120,120,120),50)
        self.text_over_6 = Text("Back to menu",(0,0,0),50)

        self.crash_sound_effect = SoundEffect("crash_sound.wav") #check_collision_object #
        self.omg_sound_effect = SoundEffect("omg_sound.wav") #
        self.omg_ness_sound_effect = SoundEffect("omg_ness_sound.wav") #
        self.wtf_sound_effect = SoundEffect("wtf_sound.wav") #
        self.doo_doo_sound_effect = SoundEffect("doo_doo_sound.wav") #
        self.mmhm_sound_effect = SoundEffect("mmhm_sound.wav") #
        self.myeah_sound_effect = SoundEffect("myeah_sound.wav") #
        self.baby_fart_sound_effect = SoundEffect("baby_fart_sound.wav") #
        self.gem_sound_effect = SoundEffect("gem_sound.wav") #
        self.lol_sound_effect = SoundEffect("lol_sound.wav") #
        self.super_item_sound_effect = SoundEffect("super_item_sound.wav") #
        self.hoho_sound_effect = SoundEffect("hoho_sound.wav") #
        self.golden_sound_effect = SoundEffect("golden_sound.wav") #
        
        self.snake = Snake()
        self.classic_item = ClassicItem()
        self.super_item = SuperItem()
        self.map = Map()

    def event_quit(self,event) :
        if event.type == pygame.QUIT :
            pygame.quit()
            quit()

    def event_handler(self,event) :
        if event.type == pygame.QUIT :
            pygame.quit()
            quit()
        if event.type == pygame.KEYDOWN :
            if event.key == pygame.K_p :
                self.song.pause()
                pygame.mixer.music.pause()
                self.game_pause = True
                self.pause()
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN or event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT :
                self.snake.event_handler(event)

    def check_collision(self) :
        def crash_damage_life(sound_effect1,sound_effect2,sound_effect3 = 0) :
            self.crash_sound_effect.play()
            randSound = random.randrange(0,3)
            if randSound == 0 :
                sound_effect1
            elif randSound == 1 :
                sound_effect1
            else :
                self.crash_sound_effect.play()
                sound_effect3
        def empty_life() :
            self.song.pause()
            self.score_image.render((0,600))
            self.text_score.to_screen_normal(40,612)
            self.text_score = Text("SCORE %d" %(self.score),(0,0,0),35)
            self.text_life.to_screen_normal(540,612)
            pygame.display.flip()
            time.sleep(2)
            self.game_start = False
            self.game_over = True
        
        index_classic_item = self.snake.positions[0].collidelist(self.classic_item.positions)
        index_super_item = self.snake.positions[0].collidelist(self.super_item.positions)
        index_classic_wall = self.snake.positions[0].collidelist(self.map.classic_wall.positions)
        index_rotate_wall = self.snake.positions[0].collidelist(self.map.rotate_wall.positions)
        if index_classic_item == 0 :
            self.gem_sound_effect.play()
            randSound = random.randrange(0,6)
            if randSound == 1 :
                self.baby_fart_sound_effect.play()
            elif randSound == 5 :
                self.hoho_sound_effect.play()
            self.classic_item.positions.pop(index_classic_item)
            self.snake.longer()
            self.score += 100
            self.eat_item += 1
            self.super_item.positions = []
        elif index_super_item == 0 :
            randEffect = random.randrange(0,3)
            if randEffect == 0 :
                self.super_item_sound_effect.play()
                self.lol_sound_effect.play()
                for i in range(0,3) :
                    self.snake.shorter()
            elif randEffect == 1 :
                self.doo_doo_sound_effect.play()
                for i in range(0,3) :
                    self.snake.longer()
            else :
                self.super_item_sound_effect.play()
                self.golden_sound_effect.play()
                self.score += 500
            self.super_item.positions.pop(index_super_item)
            self.score += 1000
            self.eat_item += 1
        elif index_classic_wall >= 0:
            crash_damage_life(self.wtf_sound_effect.play(),self.omg_sound_effect.play(),self.omg_ness_sound_effect.play())
            empty_life()
        elif index_rotate_wall >= 0 :
            crash_damage_life(self.wtf_sound_effect.play(),self.omg_sound_effect.play(),self.omg_ness_sound_effect.play())
            empty_life()
        new_rect = self.snake.positions.pop(0)
        index_snake_body = new_rect.collidelist(self.snake.positions)
        if index_snake_body >= 1 :
            crash_damage_life(self.mmhm_sound_effect.play(),self.myeah_sound_effect.play())
            if len(self.life.positions) != 0 :
                self.life.shorter()
                self.snake.positions.insert(0, new_rect)
                if len(self.life.positions) == 0 :
                    empty_life()
        else :
            self.snake.positions.insert(0, new_rect)
        #self.text_score = Text("SCORE %d" %(self.score),(0,0,0),35)

    def render(self) :
        self.background.render((0,0))
        self.classic_item.render()
        self.super_item.render()
        self.snake.render()
        self.map.render()
        self.score_image.render((0,600))
        self.life.render()
        self.text_score = Text("SCORE %d" %(self.score),(0,0,0),35)
        self.text_score.to_screen_normal(40,612)
        self.text_life.to_screen_normal(540,612)
        pygame.display.flip()
    
    def main(self) :
        while self.game_main :
            self.set_mode = 0
            for event in pygame.event.get() :
                self.event_quit(event)
            self.background.render((0,0))
            self.main_image.render((200,150))
            self.text_main_1.to_screen_center(-220)
            curser = Curser()
            if 325 < curser.cur[0] < 470 and 400 < curser.cur[1] < 450 :
                self.text_main_2.to_screen_center(100)
                if curser.click[0] == 1 :
                    time.sleep(0.2)
                    self.game_main = False
                    self.game_mode = True
            else :
                self.text_main_3.to_screen_center(100)
            if 315 < curser.cur[0] < 480 and 475 < curser.cur[1] < 525 :
                self.text_main_4.to_screen_center(180)
                if curser.click[0] == 1 :
                    time.sleep(0.2)
                    self.game_main = False
                    self.game_how = True
            else :
                self.text_main_5.to_screen_center(180)
            if 365 < curser.cur[0] < 420 and 555 < curser.cur[1] < 595 :
                self.text_main_6.to_screen_center(260)
                if curser.click[0] == 1 :
                    time.sleep(0.2)
                    pygame.quit()
                    quit()
            else :
                self.text_main_7.to_screen_center(260)
            pygame.display.update()

    def mode(self) :
        while self.game_mode :
            for event in pygame.event.get() :
                self.event_quit(event)
            self.background.render((0,0))
            self.text_mode_1.to_screen_center(-220)
            curser = Curser()
            if (90 < curser.cur[0] < 195 and 190 < curser.cur[1] < 235) or (45 < curser.cur[0] < 245 and 270 < curser.cur[1] < 460) :
                self.text_mode_3.to_screen_normal(100,200)
                self.map_1_c_image.render((50,280))
                if curser.click[0] == 1 :
                    self.mode_sound_effect.play()
                    time.sleep(1.2)
                    self.game_mode = False
                    self.game_start = True
                    self.set_mode = 1
            else :
                self.text_mode_2.to_screen_normal(100,200)
                self.map_1_image.render((50,280))
            if (360 < curser.cur[0] < 435 and 190 < curser.cur[1] < 235) or (295 < curser.cur[0] < 495 and 270 < curser.cur[1] < 460) :
                self.text_mode_5.to_screen_center(-100)
                self.map_2_c_image.render((300,280))
                if curser.click[0] == 1 :
                    self.mode_sound_effect.play()
                    time.sleep(1.2)
                    self.game_mode = False
                    self.game_start = True
                    self.set_mode = 2
            else :
                self.text_mode_4.to_screen_center(-100)
                self.map_2_image.render((300,280))
            if (590 < curser.cur[0] < 700 and 190 < curser.cur[1] < 235) or (545 < curser.cur[0] < 750 and 270 < curser.cur[1] < 460) :
                self.text_mode_7.to_screen_normal(600,200)
                self.map_3_c_image.render((550,280))
                if curser.click[0] == 1 :
                    self.mode_sound_effect.play()
                    time.sleep(1.2)
                    self.game_mode = False
                    self.game_start = True
                    self.set_mode = 3
            else :
                self.text_mode_6.to_screen_normal(600,200)
                self.map_3_image.render((550,280))
            if 345 < curser.cur[0] < 450 and 525 < curser.cur[1] < 550 :
                self.text_mode_8.to_screen_center(220)
                if curser.click[0] == 1 :
                    self.game_mode = False
                    self.game_main = True
            else:
                self.text_mode_9.to_screen_center(220)
            pygame.display.update()

    def how(self) :
        while self.game_how :
            for event in pygame.event.get() :
                self.event_quit(event)
            self.background.render((0,0))
            self.text_how_1.to_screen_center(-220)
            self.text_how_2.to_screen_center(-120)
            self.text_how_3.to_screen_center(-70)
            self.text_how_4.to_screen_center(-20)
            self.text_how_5.to_screen_center(30)
            self.text_how_6.to_screen_center(80)
            self.text_how_7.to_screen_center(130)
            self.text_how_8.to_screen_center(180)
            self.text_how_9.to_screen_center(230)
            curser = Curser()
            if 65 < curser.cur[0] < 165 and 590 < curser.cur[1] < 620 :
                self.text_how_10.to_screen_normal(40,580)
                if curser.click[0] == 1 :
                    self.game_how = False
                    self.game_main = True
            else :
                self.text_how_11.to_screen_normal(70,600)
            pygame.display.update()

    def pause(self) :
        while self.game_pause :
            for event in pygame.event.get() :
                self.event_quit(event)
            self.background.render((0,0))
            self.pause_image.render((200,210))
            self.text_pause_1.to_screen_center(-180)
            curser = Curser()
            if 195 < curser.cur[0] < 305 and 460 < curser.cur[1] < 505 :
                self.text_pause_2.to_screen_normal(200,470)
                if curser.click[0] == 1 :
                    self.game_pause = False
                    self.song.unpause()
            else :
                self.text_pause_3.to_screen_normal(200,470)
            if 415 < curser.cur[0] < 605 and 460 < curser.cur[1] < 505 :
                self.text_pause_4.to_screen_normal(420,470)
                if curser.click[0] == 1 :
                    time.sleep(0.2)
                    self.game_pause = False
                    self.game_start = False
                    self.game_main = True
                    self.song.load("game_main_sound.wav") 
                    self.song.play()
            else:
                self.text_pause_5.to_screen_normal(420,470)
            pygame.display.update()
        
    def set_map(self) :
        if self.set_mode == 1 :
            self.map.mode1()
            self.song.load("normal_sound.wav")
            self.song.play()
        elif self.set_mode == 2 :
            self.map.mode2()
            self.song.load("hard_sound.wav")
            self.song.play()
        elif self.set_mode == 3 :
            self.map.mode3()
            self.song.load("insane_sound.wav")
            self.song.play()

    def start(self) :
        self.song.load("game_main_sound.wav")
        self.song.play(-1)
        while not self.game_exit : #score no change !!
            self.score = 0
            self.eat_item = 0
            head = self.snake.top.rect.move(400, 300)
            body = self.snake.mid.rect.move(head.x - self.snake.top.rect.width, 300)
            tail = self.snake.bot.rect.move(body.x - self.snake.mid.rect.width, 300)
            self.snake.positions = [head, body, tail]
            self.snake.colors = [pygame.transform.rotate(self.snake.top.surface, 270), self.snake.mid.surface, self.snake.bot.surface]
            self.snake.directions = [1, 0]
            self.classic_item.positions = []
            self.super_item.positions = []
            self.map.classic_wall.positions = []
            self.map.rotate_wall.positions = []
            life_1 = self.life.one.rect.move(620,612)
            life_2 = self.life.two.rect.move(life_1.x + self.life.one.rect.width + 10, 612)
            life_3 = self.life.three.rect.move(life_2.x + self.life.one.rect.width + 10, 612)
            self.life.positions = [life_1, life_2, life_3]
            self.life.colors = [self.life.one.surface,self.life.two.surface,self.life.three.surface]
            self.main()
            self.mode()
            self.how()
            self.set_map()
            while self.game_start :
                self.classic_item.setup(self.map.classic_wall.positions,self.map.rotate_wall.positions,self.snake.positions)
                if self.eat_item % 5 == 0 and self.eat_item != 0 :
                    self.super_item.setup(self.classic_item.positions,self.map.classic_wall.positions,self.map.rotate_wall.positions,self.snake.positions)
                self.clock.tick(12)
                for event in pygame.event.get():
                    self.event_handler(event)
                    self.snake.rotate()
                self.snake.move()
                self.check_collision()
                self.render()
            self.over()

    def over(self) :
        while self.game_over :
            for event in pygame.event.get() :
                self.event_quit(event)
            self.background.render((0,0))
            self.text_over_1.to_screen_center(-220)
            self.over_image.render((200,150))
            self.text_over_2 = Text("Your score : %d" %self.score,(0,0,0),50)
            self.text_over_2.to_screen_center(100)
            curser = Curser()
            if 195 < curser.cur[0] < 340 and 490 < curser.cur[1] < 540 :
                self.text_over_3.to_screen_normal(200,500)
                if curser.click[0] == 1 :
                    self.game_over = False
                    self.game_start = True
            else :
                self.text_over_4.to_screen_normal(200,500)
            if 415 < curser.cur[0] < 605 and 490 < curser.cur[1] < 540 :
                self.text_over_5.to_screen_normal(420,500)
                if curser.click[0] == 1 :
                    time.sleep(0.2)
                    self.game_over = False
                    self.game_main = True
                    self.song.load("game_main_sound.wav")
                    self.song.play(-1)
            else :
                self.text_over_6.to_screen_normal(420,500)
            pygame.display.update()

if __name__ == "__main__" :
    feedthesnake = Game()
    feedthesnake.start()
